calltodf <- function(modelo) {
    call <- modelo$call

    vector_call <- paste(call)

    if (length(vector_call) == 4) {
        # Modelo sin reg y sin méthod

        df <- data.frame(
            modelo = paste(
                vector_call[1],
                gsub("c", "", vector_call[3]),
                gsub("c","", vector_call[4])
            ),
            reg = "N/C",
            Q12 = Box.test(modelo$residuals,12,type = "Ljung-Box")$statistic,
            Q12p = Box.test(modelo$residuals, 12, type = "Ljung-Box")$p.value,
            Q24 = Box.test(modelo$residuals,24,type = "Ljung-Box")$statistic,
            Q24p = Box.test(modelo$residuals, 24, type = "Ljung-Box")$p.value,
            W = shapiro.test(modelo$residuals)$statistic,
            Wp = shapiro.test(modelo$residuals)$p.value
        )

    }

    if (length(vector_call) > 4) {
        df <- data.frame(
            modelo = paste(
                vector_call[1],
                gsub("c", "", vector_call[3]),
                gsub("c", "", vector_call[4])
            ),
            reg = vector_call[5],
            Q12 = Box.test(modelo$residuals, 12, type = "Ljung-Box")$statistic,
            Q12p = Box.test(modelo$residuals, 12, type = "Ljung-Box")$p.value,
            Q24 = Box.test(modelo$residuals, 24, type = "Ljung-Box")$statistic,
            Q24p = Box.test(modelo$residuals, 24, type = "Ljung-Box")$p.value,
            W = shapiro.test(modelo$residuals)$statistic,
            Wp = shapiro.test(modelo$residuals)$p.value
        )
    }

    df


}


library(purrr)

ls(
    pattern = "modelo"
) %>%
map_dfr(
    .f = function(x) {
        
        glue(
            "
                calltodf(
                    {x}
                )
            ",
            x = x
        ) %>%
        rlang::parse_expr() %>%
        rlang::eval_tidy()
    }
) %>%
View()